<?php

namespace App\Http\Controllers;

use App\Question;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Gate;

use App\Http\Requests\Questions\CreateQuestionRequest;
use App\Http\Requests\Questions\UpdateQuestionRequest;

class QuestionsController extends Controller
{
    
    public function __construct(){
        $this->middleware(['auth'])->only(['create','store','edit','update']);
    }

    public function index()
    {
        // Question::with('owner') this we did, it is knwon as , we did this because in index.php mera owner name woh n+1 times query chala raha tha (without with lazy load karta hai)
        $questions = Question::with('owner')->latest()->paginate(10);
        return view('questions.index', compact([
            'questions'
        ]));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //Gate m humesha n-1 args pass karege hum kyuki woh use khudi deta hai
        
            app('debugbar')->disable();
            return view('questions.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateQuestionRequest $request)
    {
        auth()->user()->questions()->create([
            'title' => $request->title,
            'body' => $request->body
        ]);

        session()->flash('success','Question has been added successfully!');
        return redirect(route('questions.index'));
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Question  $question
     * @return \Illuminate\Http\Response
     */
    // public function show($slug)
    // {
    //     $question = Question::where('slug',$slug)->firstOrFail();
    // }
    public function show(Question $question)
    {
        $question->increment('views_count');
        return view('questions.show',compact([
            'question'
        ]));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function edit(Question $question)
    {
        if($this->authorize('update',$question)){
            app('debugbar')->disable();
            return view('questions.edit', compact([
                'question'
            ]));
        }

        abort(403);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateQuestionRequest $request, Question $question)
    {
        if($this->authorize('update',$question)){
            $question->update([
                'title'=>$request->title,
                'body'=>$request->body
            ]);
            session()->flash('success','Question has been updated successfully!');
            return redirect(route('questions.index'));
        }

        abort(403);

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Question  $question
     * @return \Illuminate\Http\Response
     */
    public function destroy(Question $question)
    {
        // if(Gate::allows('delete-question',$question)){
        //     $question->delete();
        //     session()->flash('success','Question has been deleted successfully!');
        //     return redirect(route('questions.index'));
        // }

        // abort(403);

        // if(auth()->user()->can('delete-question',$question)){
        //     $question->delete();
        //      session()->flash('success','Question has been deleted successfully!');
        //      return redirect(route('questions.index'));
        // }

        if($this->authorize('delete',$question)){
            $question->delete();
             session()->flash('success','Question has been deleted successfully!');
             return redirect(route('questions.index'));
        }

        abort(403);
    }
}
