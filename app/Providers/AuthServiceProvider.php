<?php

namespace App\Providers;

use App\Question;
use App\Answer;
use App\Policies\QuestionsPolicy;
use App\Policies\AnswerPolicy;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        // 'App\Model' => 'App\Policies\ModelPolicy',
        Question::class => QuestionsPolicy::class,
        Answer::class => AnswerPolicy::class
         //boot method khud ye policy ko register kardeta hai
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        //this function define() will automatically pass the current signed in user in $user
        // Gate::define('update-question', function($user,$question){
        //     return $user->id === $question->user_id;
        // });
        // Gate::define('delete-question', function($user,$question){
        //     return $user->id === $question->user_id && $question->answers_count === 0;
        // });
    }
}
